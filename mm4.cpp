//-Tercera version multiplicacion de matrices (Paralelización)
#include <omp.h>
#include "matrix.hh"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

void Happymult(const Matrix &A, const Matrix &B, Matrix &C)
{
    size_t i, j, k;
#pragma omp parallel shared(A, B, C) private(i, j, k)
    {
#pragma omp for schedule(static)
        for (i = 0; i < A.numRows(); i++)
        {
            for (j = 0; j < A.numRows(); j++)
            {
                for (k = 0; k < A.numRows(); k++)
                {
                    if (A.at(i, k) == inf || B.at(k, j) == inf)
                        C.at(i, j) = min(C.at(i, j), inf);
                    else
                        C.at(i, j) = min(C.at(i, j), A.at(i, k) + B.at(k, j));
                }
            }
        }
}}

void happyn(const Matrix &A, Matrix &B, int n)
{
    Matrix C(A.numRows(), A.numCols());
    B = A;
    size_t i;
#pragma omp parallel shared(A, B, C) private(i)
    {
#pragma omp for schedule(static)
        for (i = 0; i < n; i++)
        {
            Happymult(A, B, C);
            B = C;
        }
    }
}

void Happylog(const Matrix &A, Matrix &C)
{

    if (A.numRows() == 0)
    {
        //identidad
        C.identidad();
    }
    else
    {
        if (A.numRows() == 1)
        {
            // A
            C = A;
        }
        else
        {
            if (A.numRows() % 2 == 0)
            {
                // (A*A)^(n/2)
                Matrix J(A.numRows(), A.numCols());
                Happymult(A, A, J);
                happyn(J, C, A.numRows() / 2);
            }
            else
            {
                // A*(A*A)^((n-1)/2)
                Matrix J(A.numRows(), A.numCols());
                Matrix K(A.numRows(), A.numCols());
                Happymult(A, A, J);
                happyn(J, K, (A.numRows() - 1) / 2);
                Happymult(A, K, C);
            }
        }
    }
}
void readFile(string fileName, Matrix &C)
{

    string line;
    char str[1024];
    char *pch;

    int datos[3];
    int i, cont = 0;

    ifstream infile;
    infile.open(fileName);

    while (!infile.eof())
    {
        getline(infile, line);
        strcpy(str, line.c_str());

        pch = strtok(str, " ");
        while (pch != NULL)
        {
            i = atoi(pch);
            datos[cont] = i;
            pch = strtok(NULL, " ");
            cont++;
            if (cont == 3)
                cont = 0;
        }

        C.at(datos[0], datos[1]) = datos[2];
    }

    infile.close();
}

int main()
{
    string archivo = "p2";
    cout << "Ingrese el número de nodos: ";
    int n;
    cin >> n;
    Matrix A(n, n);
    Matrix B(n, n);
    readFile(archivo, A);
    //A.fill();
    //cout << '\n';
    A.print();
    cout << '\n';

    for (int i = 0; i < 10; i++)
    {
        double start = omp_get_wtime();
        Happylog(A, B);
        double time1 = omp_get_wtime() - start;
        cout << time1 << '\n';
    }
        B.print();
}