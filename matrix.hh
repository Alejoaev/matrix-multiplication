#ifndef __MATRIX_HH__
#define __MATRIX_HH__

#include <vector>
#include <iostream>
#include <random>
#include <stdlib.h>
#include <algorithm>

using std::cout;
using std::endl;
using std::mt19937;
using std::random_device;
using std::uniform_real_distribution;
using std::vector;

const double inf = std::numeric_limits<double>::max();
class Matrix
{
private:
  vector<double> data;
  size_t rows;
  size_t cols;

public:
  Matrix(size_t r, size_t c)
  {
    rows = r;
    cols = c;
    data.resize(r * c, inf);
  }

  size_t numRows() const { return rows; }
  size_t numCols() const { return cols; }

  double at(size_t i, size_t j) const
  {
    size_t idx = cols * i + j;
    return data[idx];
  }

  double &at(size_t i, size_t j)
  {
    size_t idx = cols * i + j;
    return data[idx];
  }

  bool operator==(const Matrix &rhs) const
  {
    if (rows != rhs.rows)
      return false;
    if (cols != rhs.cols)
      return false;
    for (size_t i = 0; i < data.size(); i++)
      if (data[i] != rhs.data[i])
        return false;
    return true;
  }

  void identidad()
  {
    for (size_t i = 0; i < numRows(); i++)
    {
      for (size_t j = 0; j < numCols(); j++)
      {
        if (i == j)
          at(i, j) = 1;
        else
          at(i, j) = 0;
      }
    }
  }
  void fill2()
  {
    for (size_t i = 0; i < data.size(); i++)
    {
      data[i] = 2;
    }
  }
  void prueba1()
  {
    data =
        {0, 6, inf, 9, inf, inf, inf, inf,
         inf, 0, inf, inf, inf, 6, 3, inf,
         inf, inf, 0, inf, 5, inf, 3, inf,
         inf, inf, inf, 0, inf, inf, inf, inf,
         inf, inf, inf, inf, 0, inf, inf, 8,
         inf, 5, inf, 6, inf, 0, inf, 7,
         inf, inf, inf, inf, 4, inf, 0, 3,
         inf, inf, inf, inf, inf, inf, inf, 0};
  }
  void prueba()
  {
    data = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  }

  void fill()
  {
    random_device rd;
    mt19937 gen(rd());
    uniform_real_distribution<> dis(1.0, 2.0);
    for (size_t i = 0; i < data.size(); i++)
    {
      data[i] = dis(gen);
    }
  }

  void print()
  {

    for (size_t i = 0; i < numRows(); i++)
    {
      for (size_t j = 0; j < numCols(); j++)
        cout << at(i, j) << ' ';
      cout << '\n';
    }
  }

  // Calcula el diametro del grafo
  size_t Diametro()
  {
    size_t max = 0;
    size_t maxv;
    for (size_t i = 0; i < data.size(); i++)
    {
       maxv = data[i];
      if (maxv >= max && maxv != inf)
        max = maxv;
    }
    return max;
  }
};

#endif
