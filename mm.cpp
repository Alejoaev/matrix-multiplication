//Primera version multiplicacion de matrices (vector de vectores)

#include <iostream>
#include <vector>
#include <iomanip>
#include <limits>
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>
#include <cassert>
#include <string.h>
#include <stdlib.h>

using namespace std;

const int g = numeric_limits<int>::max(); // Infinito

// imprime la matriz
void print(const vector<vector<int>>& C){
    for ( int i = 0; i < C.size(); i++ ){
        for ( int j = 0; j < C[0].size(); j++ )
          cout<< setw ( 10 ) << C[i][j] <<' ';
        cout<<'\n';
    }
}

int suma(int a, int b){
    if(a == g || b == g) return g;
    else return a+b;
}

// Se genera la matriz identidad en C
void identidad(const vector<vector<int>>& A, vector<vector<int>>& C){
    for(int i=0;i<A.size();i++){
        for(int j=0;j<A[0].size();j++){
            if (i == j) C[i][j] = 1;
            else C[i][j] = 0;
        } 
    } 
}

// Calcula el diametro del grafo
int Diametro(const vector<vector<int>>& C){
    int max = 0;
    for(int i=0;i < C.size(); i++){
         int maxv = *max_element(C[i].begin(), C[i].end());
         if (maxv >= max && maxv != g) max = maxv ;
  }
    return max;
}
// multiplicacion A*B y el resultado se guarda en C
void matrixmult(const vector<vector<int>>& A, const vector<vector<int>>& B,vector<vector<int>>& C){
    int rows_A = A.size();
    int cols_A = A[0].size();
    int rows_B = B.size();
    int cols_B = B[0].size();

    // Se llena la matriz C con infinito
    for(int y=0;y<rows_A;y++){
        for(int x=0;x<cols_B;x++){
            C[y][x]=g;
        }
    }

    // se realiza la multiplicacion "rara"    
    for(int i=0;i<rows_A;i++){
        for(int j=0;j<cols_B;j++){
            for(int k=0;k<cols_A;k++)
            C[i][j] = min(C[i][j], suma(A[i][k], B[k][j]));
        }
    }
}

// Funcion extraña para multiplicar A^n
void strange(const vector<vector<int>>& A,vector<vector<int>>& C){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    C = A;
    for (int i = 0; i < A.size(); i++){
         matrixmult(A,C,J);
         C = J;
    }
}

// Funcion extraña para multiplicar A^(a cualquier exponente)
void strangeN(const vector<vector<int>>& A,vector<vector<int>>& C,int n){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    C = A;
    for (int i = 0; i < n; i++){
         matrixmult(A,C,J);
         C = J;
    }
}

// Funcion extraña2 para multiplicar A^n complejidad logaritmica
void strange2(const vector<vector<int>>& A,vector<vector<int>>& C){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    vector< vector<int>> K ( A.size(), vector<int> (A.size()) );
    if (A.size() == 0){
            //identidad
            identidad(A,C);
        }
        else {
            if (A.size() == 1){
            // A
            C = A;
        }
        else{
            if(A.size()%2 == 0){
                // (A*A)^(n/2)
                matrixmult(A,A,J);
                strangeN(J,C,A.size()/2);
            }
            else{
            // A*(A*A)^((n-1)/2)
                matrixmult(A,A,J);
                strangeN(J,K,(A.size()-1)/2);
                matrixmult(A,K,C);
            }
            }
    }
}


// inicializo la matriz
void inicializar(vector<vector<int>>& C){
    for(int y = 0; y < C.size(); y++){
        for(int x = 0; x < C[0].size(); x++){
            if(y == x) C[y][x]=0;
            else C[y][x]=g;
        }
    }

}

// Lee el archivo y lo guarda en la matriz

void readFile(string fileName,vector<vector<int>>& C) {
    
    string line;
    char str[10];
    char * pch;

    int datos[3];
    int i, cont = 0;


	ifstream infile;
	infile.open (fileName);
        while(!infile.eof()) 
        {
	        getline(infile,line); 
            strcpy(str,line.c_str());

              pch = strtok (str,"a ");
             while (pch != NULL)
                 {
                i = atoi(pch);
                datos[cont] = i;
                pch = strtok (NULL, " ");
                cont++;
                if (cont == 3) cont = 0;
             }

          C[datos[0]][datos[1]] = datos[2];
        }

	infile.close();
}

void suma(int a, int b, int& c){
    c = a+b;
}

int main() {
    //string archivo = "rome99.txt";
    //int n = 3354;

    //ejemplo para 8 nodos
    /*string archivo = "p";
    int n = 4;
    vector< vector<int>> C ( n, vector<int> (n) );
    vector< vector<int>> J ( C.size(), vector<int> (C.size()) );

    inicializar(C);
    readFile(archivo,C);
    print(C);
    vector< vector<int>> P = {C[0]};
        vector< vector<int>> M = P;
    cout << endl;
    print(P);
    matrixmult(P,C,M);
    cout << endl;
    print(M);*/
    int c = 0;
    suma(2,4,c);
    cout << c;


  /*cout<<endl <<"fase 1 "<<endl<<endl;
  strange(C,J);
  print(J);
  int D = Diametro(C);
  cout<<endl <<"El Diametro es: "<< D <<endl;*/
 /*
  cout<<endl <<"complejidad logaritmica "<<endl<<endl;
  strange2(C,J);
  print(J);
  int D = Diametro(J);
  cout<<endl <<"El Diametro es: "<< D <<endl;*/
  return 0;
}

//g++ -std=c++11 -o mult multi.cpp